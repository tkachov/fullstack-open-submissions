import React, { useState } from 'react'
import ReactDOM from 'react-dom'

function randomIndex(items) {
  return Math.floor(Math.random() * items.length)
}

function anecdoteWithMostVotes(anecdotes) {
  let maxAnecdote = anecdotes[0]

  for (const anecdote of anecdotes) {
    if (anecdote.votes > maxAnecdote.votes) {
      maxAnecdote = anecdote
    }
  }

  return maxAnecdote
}

const Button = ({ text, action }) => {
  return <button onClick={action}>{text}</button>
}

function createAnecdote(text) {
  return {
    text: text,
    votes: 0
  }
}

const initial_anecdotes = [
  createAnecdote('If it hurts, do it more often'),
  createAnecdote('Adding manpower to a late software project makes it later!'),
  createAnecdote('The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.'),
  createAnecdote('Any fool can write code that a computer can understand. Good programmers write code that humans can understand.'),
  createAnecdote('Premature optimization is the root of all evil.'),
  createAnecdote('Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.'),
]

const App = () => {
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [anecdotes, setAnecdotes] = useState(initial_anecdotes)

  const randomizeSelected = (anecdotes) => setSelectedIndex(randomIndex(anecdotes))
  const incrementVotes = (anecdotes, index) => {
    const copy = [...anecdotes]
    copy[index].votes += 1
    setAnecdotes(copy)
  }

  const maxAnectode = anecdoteWithMostVotes(anecdotes)

  return (
    <div>
      <h1>Anecdote of the day</h1>
      <div>
        {anecdotes[selectedIndex].text}
      </div>
      <Button text="vote" action={() => incrementVotes(anecdotes, selectedIndex)} />
      <Button text="next anecdote" action={() => randomizeSelected(anecdotes)} />

      <h1>Anecdote with most votes</h1>
      <div>{maxAnectode.text}</div>
      <div>Votes: {maxAnectode.votes}</div>
    </div>
  )
}


ReactDOM.render(
  <App />,
  document.getElementById('root')
)
