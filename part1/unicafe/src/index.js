import React, { useState } from 'react'
import ReactDOM from 'react-dom'

const Button = ({ action, text }) => {
  return <button onClick={action}>{text}</button>
}

const Statistic = ({ text, value }) => {
  return (
    <tr>
      <td>{text}</td>
      <td>{value}</td>
    </tr>
  )
}

const Statistics = ({ good, neutral, bad, counter }) => {
  let content;

  if (counter === 0) {
    content = <div>No feedback given</div>
  } else {
    const avg = (good - bad) / counter
    const positive = good / counter * 100 + " %"

    content = (
      <table>
        <tbody>
          <Statistic text="good" value={good} />
          <Statistic text="neutral" value={neutral} />
          <Statistic text="bad" value={bad} />
          <Statistic text="all" value={counter} />
          <Statistic text="average" value={avg} />
          <Statistic text="positive" value={positive} />
        </tbody>
      </table>
    )
  }

  return (
    <>
      <h1>statistics</h1>
      {content}
    </>
  )
}

const App = () => {
  // save clicks of each button to own state
  const [good, setGood] = useState(0);
  const [neutral, setNeutral] = useState(0);
  const [bad, setBad] = useState(0);
  const [counter, setCounter] = useState(0);

  const createIncrementer = (value, setter) => {
    return () => {
      setter(value + 1);
      setCounter(counter + 1);
    }
  }

  return (
    <div>
      <h1>give feedback</h1>
      <Button text={"good"} action={createIncrementer(good, setGood)} />
      <Button text={"neutral"} action={createIncrementer(neutral, setNeutral)} />
      <Button text={"bad"} action={createIncrementer(bad, setBad)} />
      <Statistics good={good} neutral={neutral} bad={bad} counter={counter} />
    </div>
  )
}

ReactDOM.render(<App />,
  document.getElementById('root')
)
