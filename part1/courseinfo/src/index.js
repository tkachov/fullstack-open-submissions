import React from 'react'
import ReactDOM from 'react-dom'

const Header = (props) => {
  console.log(props);
  return (
    <h1>{props.name}</h1>
  )
}

const Part = (props) => {
  return (
    <p>
      {props.name} {props.exercises}
    </p>
  )
}

const Content = (props) => {
  const parts = props.parts.map(part => <Part name={part.name} exercises={part.exercises}/>);
  return (
    <>
      {parts}
    </>
  )
}

const Total = (props) => {
  const sum = props.parts.reduce((acc, part) => acc + part.exercises, 0);
  return (
    <p>Number of exercises {sum}</p>
  )
}

const Course = (props) => {
  const course = props.course;
  return (
    <div>
      <Header name={course.name} />
      <Content parts={course.parts} />
      <Total parts={course.parts} />
    </div>
  )
}

const App = () => {
  const course = {
    name: 'Half Stack application development',
    parts: [
      {
        name: 'Fundamentals of React',
        exercises: 10
      },
      {
        name: 'Using props to pass data',
        exercises: 7
      },
      {
        name: 'State of a component',
        exercises: 14
      }
    ]
  }

  return <Course course={course}/>
}

ReactDOM.render(<App />, document.getElementById('root'))